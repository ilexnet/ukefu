package com.ukefu.webim.web.handler.resouce;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ukefu.util.Menu;
import com.ukefu.webim.web.handler.Handler;

@Controller
@RequestMapping("/res")
public class ImageController extends Handler{
	
	@Value("${web.upload-path}")
    private String path;
	
    @RequestMapping("/image")
    @Menu(type = "resouce" , subtype = "image" , access = true)
    public void index(HttpServletResponse response, @Valid String id) throws IOException {
    	File file = new File(path ,id) ;
    	if(file.exists() && file.isFile()){
    		response.getOutputStream().write(FileUtils.readFileToByteArray(new File(path ,id)));
    	}
    }
    
    @RequestMapping("/url")
    @Menu(type = "resouce" , subtype = "image" , access = true)
    public void url(HttpServletResponse response, @Valid String url) throws IOException {
    	byte[] data = new byte[1024] ;
    	int length = 0 ;
    	OutputStream out = response.getOutputStream();
    	if(!StringUtils.isBlank(url)){
    		InputStream input = new URL(url).openStream() ;
    		while((length = input.read(data) )> 0){
    			out.write(data, 0, length);
    		}
    		input.close();
    	}
    }
    
}